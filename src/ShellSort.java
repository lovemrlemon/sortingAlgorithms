/*
In shell sort, elements at a specific interval are sorted.
The interval between the elements is gradually decreased based on the
sequence used. In the last phase, insertion sort is used.

shell's original sequence: N/2 , N/4, .. , 1
Knuth's increments: 1, 4, 13, ... , (3k - 1) / 2
...
https://www.programiz.com/dsa/shell-sort

Sedgewick proposed several increment sequences that give an
O(N^4/3) worst-case running time.

** imagine the logic you used for priority queues.
you first store he value you want to move in a temp variable,
create a hole there, shift the other elements, then put the value
where the hole ends up.
 */

public class ShellSort {

    // counter to count the total number of comparisons made
    static int counter = 0;

    public static void main(String[] args) {
        int[] array;

        array = RandomArrayGenerator.intArray();

        //System.out.println("before: " + Arrays.toString(array));

        long start = System.currentTimeMillis();

        shellSort(array);

        long end = System.currentTimeMillis();

        //System.out.println("after: " + Arrays.toString(array));

        System.out.println("Number of comparisons made: " + counter);
        System.out.println("It took " + (end - start) + " ms.");
    }

    public static void shellSort(int[] array) {
        int temp;
        int len = RandomArrayGenerator.arrayLength;

        // calculates the gap each time, and takes us to the first element of this phase
        for (int gap = len / 2; gap > 0; gap /= 2){
            counter ++;
            // iterates through every element after the gap index
            for (int i = gap; i < len; i ++){
                counter++;

                // save array[i] in temp and make a hole at position i
                temp = array[i];

                int j;
                // shift earlier elements up until the correct location for a[i] is found.
                for (j = i; j >= gap && array[j - gap] > temp; j -= gap){
                    //System.out.println("current item: " + temp + "  compared to : " + array[j - gap]);
                    counter += 2;
                    array[j] = array [j - gap];

                }
                // put temp (the content of the hole) in its correct location
                array[j] = temp;
            }
        }
    }
}
