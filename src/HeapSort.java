/*
Priority queues can be used to sort in O(NlogN) time.
But in practice, it is slower than a version of shell sort
that uses Sedgewick increments.

Its performance is quite consistent.

First you turn your array into a max heap.
Then take the root (max element), exchange it with the last element,
heapify, repeat.

https://www.programiz.com/dsa/heap-sort
 */

public class HeapSort {

    // counter to count the total number of comparisons made
    static int counter = 0;

    public static void main(String[] args) {
        int[] array;

        array = RandomArrayGenerator.intArray();

        //System.out.println("before: " + Arrays.toString(array));

        long start = System.currentTimeMillis();

        heapSort(array);

        long end = System.currentTimeMillis();

        //System.out.println("after: " + Arrays.toString(array));

        System.out.println("Number of comparisons made: " + counter);
        System.out.println("It took " + (end - start) + " ms.");
    }

    public static void heapSort(int[] array) {
        int len = RandomArrayGenerator.arrayLength;

        buildMaxHeap(array, len);

        int N = len - 1;

        while (N >= 0) {
            counter ++;

            swapItems(array, 0, N);
            heapify(array, N, 0);
            N --;
            //System.out.println("N: " + N + "   " + Arrays.toString(array));
        }
    }
    // build heap (rearrange the original array)
    public static void buildMaxHeap(int[] array, int len) {
        for (int i = len / 2 - 1; i >= 0; i--) {
            heapify(array, len, i);
        }
    }

    public static void heapify(int[] array, int len, int i) {
        int left = 2 * i + 1;
        int right = 2 * i + 2;
        int largest = i;

        counter += 2;
        if (left < len && array[left] > array[largest]) {
            largest = left;
        }
        counter += 2;
        if (right < len && array[right] > array[largest]) {
            largest = right;
        }

        counter++;
        if (largest != i) {
            swapItems(array, largest, i);
            heapify(array, len, largest);
        }
    }

    public static void swapItems(int[] array, int index1, int index2){
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
