import java.util.Arrays;

public class CountSort {

    // counter to count the total number of comparisons made
    static int counter = 0;

    public static void main(String[] args) {
        int[] array;

        array = RandomArrayGenerator.intArray();

        //System.out.println("before: " + Arrays.toString(array));

        long start = System.currentTimeMillis();

        countSort(array);

        long end = System.currentTimeMillis();

        //System.out.println("after: " + Arrays.toString(array));

        System.out.println("Number of comparisons made: " + counter);
        System.out.println("It took " + (end - start) + " ms.");
    }

    public static void countSort(int[] array) {
        int len = RandomArrayGenerator.arrayLength;
        int countArrLen = findMax(array, len) + 1;
        int[] countArr = generateCountArray(countArrLen);
        fillCountArray(array, countArr, len, countArrLen);
        sortArr(array, countArr, len);
    }

    public static int findMax(int[] array, int len) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < len; i++) {
            counter += 2;
            if (array[i] > max)
                max = array[i];
        }
        //System.out.println("max: " + max);
        return max;
    }

    public static int[] generateCountArray(int len) {
        int[] arr = new int[len];
        for (int i = 0; i < len; i++) {
            counter++;
            arr[i] = 0;
        }
        return arr;
    }

    public static void fillCountArray(int[] array, int[] countArr, int len, int countArrLen) {
        for (int i = 0; i < len; i++) {
            counter++;
            countArr[array[i]] ++;
        }
        cumulativeCount(countArr, countArrLen);
    }

    public static void cumulativeCount(int[] countArr, int countArrLen) {
        for (int i = 1; i < countArrLen; i ++) {
            counter ++;
            countArr[i] += countArr[i - 1];
        }
    }

    public static void sortArr(int[] array, int[] countArr, int len) {
        int[] output = generateCountArray(len);
        for (int i = len - 1; i >= 0; i--){
            counter++;
            output[countArr[array[i]] - 1] = array[i];
            countArr[array[i]]--;
        }
        if (len >= 0) System.arraycopy(output, 0, array, 0, len);
    }
}
