/*
divide - conquer - combine ^^
 */

public class MergeSort {

    // counter to count the total number of comparisons made
    static int counter = 0;

    public static void main(String[] args) {
        int[] array;

        array = RandomArrayGenerator.intArray();

        //System.out.println("before: " + Arrays.toString(array));

        long start = System.currentTimeMillis();

        int len = RandomArrayGenerator.arrayLength;
        mergeSort(array, 0, len - 1);

        long end = System.currentTimeMillis();

        //System.out.println("after: " + Arrays.toString(array));

        System.out.println("Number of comparisons made: " + counter);
        System.out.println("It took " + (end - start) + " ms.");
    }

    /*
    p is the starting point
    r is the end
    q is the mid point
     */
    public static void mergeSort(int[] array, int p, int r) {
        counter ++;
        if (p < r) {
            counter ++;

            int q = (p + r) / 2;

            mergeSort(array, p, q);
            mergeSort(array, q + 1, r);
            merge(array, p, q, r);
        }
    }

    public static void merge(int[] array, int p, int q, int r) {

        int[] arr1 = createSubArray(array, p, q);
        int[] arr2 = createSubArray(array, q + 1, r);

        // uncomment below print to see the sub arrays at each step
        //System.out.println(Arrays.toString(arr1) + Arrays.toString(arr2));

        int len1 = q - p + 1;
        int len2 = r - q;

        mergeTwoArrays(array, arr1, arr2, len1, len2, p);
    }

    public static void mergeTwoArrays(int[] array, int[] arr1, int[] arr2,  int len1, int len2, int p) {
        int i = 0;  // maintains the index for arr1
        int j = 0;  // maintains the index for arr2
        int k = p;  // maintains the current index of array[p, ..., q]

        //until we reach the end of either of the sub arrays, we compare the corresponding items
        while (i < len1 && j < len2) {
            counter += 3;
            if (arr1[i] <= arr2[j]){
                array[k] = arr1[i];
                i++;
            } else {
                array[k] = arr2[j];
                j++;
            }
            k++;
        }
        // if we run out of elements in either arr1 or arr2, add the remaining elements
        while (i < len1) {
            counter++;
            array[k] = arr1[i];
            i++; k++;
        }
        while (j < len2) {
            counter++;
            array[k] = arr2[j];
            j++; k++;
        }
    }

    public static int[] createSubArray(int[] array, int startIndex, int endIndex) {
        int len = endIndex - startIndex + 1;
        int[] subArr = new int[len];

        for (int i = 0; i < len; i++) {
            counter++;
            subArr[i] = array[startIndex + i];
        }
        return subArr;
    }
}
