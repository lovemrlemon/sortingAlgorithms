/*
    Similar to merge sort, the idea is divide and conquer.
    1. chose a pivot element in the array (here, the pivot is the last element)
    2. compare the elements in the array with the pivot
       if an item is smaller than the pivot, it is swapped by the first larger
       item we found previously
    3. do step 2 until we reach pivotIndex - 1
    4. call pivotSort on the sub arrays before and after the pivot
    5. continue this until the sub arrays consist only of 1 item.

    https://www.programiz.com/dsa/quick-sort
 */

public class QuickSort {

    // counter to count the total number of comparisons made
    static int counter = 0;

    public static void main(String[] args) {
        int[] array;

        array = RandomArrayGenerator.intArray();

        //System.out.println("before: " + Arrays.toString(array));

        long start = System.currentTimeMillis();

        int len = RandomArrayGenerator.arrayLength;
        quickSort(array, 0, len - 1);

        long end = System.currentTimeMillis();

        //System.out.println("after: " + Arrays.toString(array));

        System.out.println("Number of comparisons made: " + counter);
        System.out.println("It took " + (end - start) + " ms.");
    }

    public static void quickSort(int[] array, int firstIndex, int lastIndex) {
        counter++;
        if (firstIndex < lastIndex) {
            int pivotIndex = partition(array, firstIndex, lastIndex);
            quickSort(array, firstIndex, pivotIndex - 1);
            quickSort(array, pivotIndex + 1, lastIndex);
        }

    }

    public static int partition(int[] array, int firstIndex, int lastIndex) {
        int pivot = array[lastIndex];
        int storeIndex = firstIndex - 1;

        for (int i = firstIndex; i < lastIndex; i++) {
            counter += 2;
            if (array[i] < pivot) {
                storeIndex++;
                swapItems(array, storeIndex, i);
            }
        }
        swapItems(array, storeIndex + 1, lastIndex);
        return storeIndex + 1;
    }

    public static void swapItems(int[] array, int index1, int index2){
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
