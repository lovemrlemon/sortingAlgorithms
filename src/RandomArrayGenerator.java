import java.util.Random;

public class RandomArrayGenerator {

    static int arrayLength = 100;
    static int randomIntBound = 10;

    public static int[] intArray() {
        int[] array = new int[arrayLength];

        Random rand = new Random();

        // every item in the array will be a random int in range 0 to 999
        for (int i = 0; i < arrayLength; i++){
            array[i] = rand.nextInt(randomIntBound);
        }
        return array;
    }
}
