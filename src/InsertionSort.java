/*
compares the item n to n + 1 in each iteration.
if they are not in order, the n + 1 is moved until it finds its place.

Because of nested loops, the worst case time complexity is O(n^2)
if input is presorted, it is O(N)
 */

public class InsertionSort {

    // counter to count the total number of comparisons made
    static int counter = 0;

    public static void main(String[] args) {

        int[] array;
        array = RandomArrayGenerator.intArray();

        //System.out.println("before: " + Arrays.toString(array));

        long start = System.currentTimeMillis();

        insertionSort(array);

        long end = System.currentTimeMillis();

        //System.out.println("after: " + Arrays.toString(array));

        System.out.println("Number of comparisons made: " + counter);
        System.out.println("It took " + (end - start) + " ms.");
    }

    public static void insertionSort(int[] array) {
        int temp;
        int len = RandomArrayGenerator.arrayLength;

        for (int i = 1; i < len; i++) {
            counter++;
            for (int j = i; j > 0 && array[j -1] > array[j]; j--) {
                counter += 2;

                //swap
                temp = array[j - 1];
                array[j - 1] = array[j];
                array[j] = temp;

                //System.out.println("process " + i + "  " + Arrays.toString(array));
            }
        }
    }
}
