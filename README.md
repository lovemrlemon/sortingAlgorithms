Thank you so much for taking time to read this!
Please do share your thoughts on what I can improve in this project. :)

## Overview

This project is an exercise to practice sorting algorithms.

It is written in JDK15 environment.

The class "RandomArrayGenerator" generates an array with random 
integers. The array length and the bound for integers can be 
easily changed by editing the ```arrayLength``` and ```randomIntBound```
variables respectively. 

Every other class has its own main function and runs independently. 

When they are run, they will tell the user the number of comparisons 
made for the sorting, and how long it took. (the timer works better for
arrays longer than 1000 items)

If the user wishes to see the array before and after the sort, they can 
uncomment the print lines in the class they want to run.